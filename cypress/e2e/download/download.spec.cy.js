describe('template spec', () => {
  it('passes', () => {
    cy.visit('/')
    cy.contains('File Download').should('exist').and('be.visible').click()
    cy.get('textarea#textbox').should('be.visible').type('This is a text file for Cohort-3')
    cy.contains('Generate File').click()
    cy.get('#link-to-download').click()
  })
})