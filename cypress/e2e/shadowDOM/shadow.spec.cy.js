context('Upload using native Cypress Command', ()=>{
    it('Using selectFile to upload jpeg', ()=>{
        cy.visit('/')
        cy.contains('Shadow DOM').should('be.visible').and('exist').click()
        cy.get('shadow-signup-form').shadow().find('[name="username"]').should('exist').type('Helen Blay')
        cy.get('shadow-signup-form').shadow().find('[name="email"]').should('exist').type('helenblay@yopmail.com')
        cy.get('shadow-signup-form').shadow().find('[name="password"]').should('exist').type('Test@1234')
        cy.get('shadow-signup-form').shadow().find('[name="confirm_password"]').should('exist').type('Test@1234')
        cy.get('shadow-signup-form').shadow().find('button[type="button"]').should('exist').click()
    })
})