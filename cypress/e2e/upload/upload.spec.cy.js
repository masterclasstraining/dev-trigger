context('Upload using native Cypress Command', ()=>{
    it('Using selectFile to upload jpeg', ()=>{
        cy.visit('/')
        cy.contains('Upload File Demo').should('be.visible').and('exist').click()
        cy.get('input[type="file"]').should('exist').selectFile('cypress/fixtures/example.jpeg')
    })
})

describe('Upload using cypress plugin', ()=>{
    it('Using attachFile to upload jpeg', ()=>{
        cy.visit('/')
        cy.contains('Upload File Demo').should('be.visible').and('exist').click()
        cy.get('input[type="file"]').should('exist').attachFile('example.jpeg')
    })
})